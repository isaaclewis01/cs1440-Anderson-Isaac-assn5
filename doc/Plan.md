Software Development Plan:

    1. Requirements:
        Program that takes a user specified website URL, starting with http (an absolute URL). This program can also
        take an optional argument on how many links to open and visit from the starting website, by default this is
        three. The program then "crawls" through a website opening non-visited links tagged by the <a> anchor tag.
        It converts a URL into an absolute URL and goes into it from there. This is done until max depth is reached
        or all links are visited.
        
    2. Design:
        1. Absolute URL is given as first command argument, maximum depth as second (optional)
            Max depth is three if it is not specified
        2. crawl() starts with a distance of 0 (webpage supplied by user), and increases everytime it crawls
        3. visited starts as an empty set, and will add the URLs it has visited when it visits them, each time after
        checking whether the crawler has already visited those sites
        4. Each time crawl() is called:
            If current value of depth exceeds maxdepth, return from crawl()
            Otherwise, fetch the webpage indicated by url
                Report any exceptions raised and proceed to next URL - program must not crash
            Scan the resulting HTML for anchor tags <a> - if the anchor tag has a href attribute
                Discard URL if it does noto specify either HTTP or HTTPS protocols
                Determine if href attribute refers to an absolute URL - if not, make it into one using urljoin()
                Discard the fragment portion of the URL, if present
            Check whether the newly-found URL has been visited before
                If it has, loop to the next anchor tag
                Otherwise record it to visited and proceed
                    Print out the URL, using indentation to indicate the current depth of recursion, output four spaces
                    for each level of recursion
                    Repeat entire process by recursively calling crawl() upon this URL
        Base Cases:
            If current depth exceeds max depth, return
            If all links have been visited that can be, return

    3. Implementation:
        Run from the bash command line, with arguments:
            Absolute URL as the first
            [Optional] maximum depth as the second, if not specified will be three

    4. Verification:
        Test one:
            Input:
                python src/crawler.py http://unnovative.net/level0.html 10
            Output:
                Crawling from http://unnovative.net/level0.html to a maximum distance of 10 links
                http://unnovative.net/level0.html
                    http://unnovative.net/level1.html
                        http://unnovative.net/level2.html
                            http://unnovative.net/level3.html
                                http://unnovative.net/level4.html
                                    http://unnovative.net/level5.html
                                        http://unnovative.net/level6.html
                                            http://unnovative.net/level7.html
                                                http://unnovative.net/level8.html
                                                    http://unnovative.net/level9.html
                                                        http://unnovative.net/level10.html
            Pass/Fail? Pass
            
        Test two:
            Input:
                python src/crawler.py testingInvalidUrl.com
            Output:
                Error: Invalid URL supplied.
                Please supply an absolute URL to this program
            Pass/Fail? Pass
