#!/usr/bin/python3


# pip install --user requests beautifulsoup4
import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse, urljoin
import sys


def crawl(url, depth, maxdepth, visited):
    spaces = '    ' * depth
    try:
        url = url.split("#")[0]

        if visited.count(url) > 0:
            return
        if depth > maxdepth:
            return

        visited.append(url)
        print(f"{spaces}{url}")

        response = requests.get(url)
        if not response.ok:
            print(f"{spaces}crawl({url}): {response.status_code} {response.reason}")

        html = BeautifulSoup(response.text, 'html.parser')
        links = html.find_all('a')
        for a in links:
            link = a.get('href')
            if link:
                # Create an absolute address from a (possibly) relative URL
                absoluteURL = urljoin(url.strip(), link.strip())

                # Only deal with resources accessible over HTTP or HTTPS
                if absoluteURL.startswith('http'):
                    crawl(absoluteURL, depth + 1, maxdepth, visited)
        return

    except Exception as e:
        print(f"{spaces}Requests or BeautifulSoup ctor() {e}")
        print(f"{spaces}Giving up on {url}")
    return


## An absolute URL is required to begin
if len(sys.argv) < 2:
    print("Error: no Absolute URL supplied")
    sys.exit(1)
else:
    url = sys.argv[1]

parsed = urlparse(url)
if parsed.scheme == '' or parsed.netloc == '':
    print("Error: Invalid URL supplied.\nPlease supply an absolute URL to this program")
    sys.exit(2)

## The user may override the default recursion depth of 3
maxDepth = 3
if len(sys.argv) > 2:
    maxDepth = int(sys.argv[2])

plural = 's'
if maxDepth == 1:
    plural = ''

print(f"Crawling from {url} to a maximum depth of {maxDepth} link{plural}")
crawl(url, 0, maxDepth, [])
